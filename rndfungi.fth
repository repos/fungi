\   ,------------.
\   |  RNDFUNGI  |
\   `------------'
\
\   OH NOES IT'S ALGORITHMIC BIAS!!!11!
\   This section takes care of creating random TX7 presets, it's not 100%
\   random though. The TX7 is designed in a way that there are quite a lot
\   of parameters combos that generate rather crappy stuff, that is when 
\   anything audible happens. There are occasional happy accidents(TM) in 
\   this vast FM territory, but such rarity can't justify having to go
\   through plethora useless presets. The cheats used to make the
\   randomness more paletable come from ancient, long forgotten TX7 sorcery,
\   combined with some personal preferences:
\   - each preset: at least one or two OP volume set to 99
\   - each OP: at least one EG level set to 99
\   - each OP: EG level 4 is always set to 0
\   - each OP: EG level 1 is always at least 50
\   - each OP: minimum 90 output level
\   - each OP: 50 mean normal random break point + hardcoded 10 scale
\   - each preset: same FM algo to avoid evolution self-sabotage
\   - each preset: no transpose
\   - each preset: evol same algo family?
\   - TODO each OP: maybe no break point + min values for other kb stuff
\                   is nice?

\ NOTE:  params 0-20 (OP1), 21-41 (OP2), 42-62 (OP3), 63-83 (OP4), 
\        84-104 (OP5), 105-125 (OP6) are following the same pattern
\        see loop in rndparent.

ANEW TASK-rndfungi

\ params 108, 107, 106, 105, OP EG rates
: rndrate ( -- c c c c )
   100 CHOOSE     \ param 108, OPn EG rate 4, 0-99
   100 CHOOSE     \ param 107, OPn EG rate 3, 0-99
   100 CHOOSE     \ param 106, OPn EG rate 2, 0-99
   100 CHOOSE     \ param 105, OPn EG rate 1, 0-99
;

\ replace with a 99 one of the 3 OP EG levels
: 99lvl1 ( l3 l2 l1 -- l3 l2 99 )   DROP 99 ;   
: 99lvl2 ( l3 l2 l1 -- l3 99 l1 )   99 ROT DROP SWAP ;   
: 99lvl3 ( l3 l2 l1 -- 99 l2 l1 )   ROT DROP 99 -ROT ;   
: 99lvlx ( n -- l3|99 l2|99 l1|99 )
   3 CHOOSE CASE
      0 OF 99lvl1 ENDOF
      1 OF 99lvl2 ENDOF
      2 OF 99lvl3 ENDOF
   ENDCASE
;

\ params 112, 111, 110, 109, OP EG levels
: rndlvl ( -- c c c c )
   0              \ param 112, OPn EG level 4, 0-99, forced to 0
   100 CHOOSE     \ param 111, OPn EG level 3, 0-99
   100 CHOOSE     \ param 110, OPn EG level 2, 0-99
   50 50 CHOOSE + \ param 109, OPn EG level 1, 0-99, at least 50
   99lvlx         \ at least one OP EG is set to 99, except level 4
;

\ params 118, 117, 116, 115, 114, 113, OP keyboard settings
\ : rndkb ( -- c c c c c c )
\   8   CHOOSE     \ param 118, OPn kb rate scaling, 0-7
\   4   CHOOSE     \ param 117, OPn kb level scaling R curve, 0-3
\   4   CHOOSE     \ param 116, OPn kb level scaling L curve, 0-3
\   100 CHOOSE     \ param 115, OPn kb level scaling R depth, 0-99
\   100 CHOOSE     \ param 114, OPn kb level scaling L depth, 0-99
\   100 CHOOSE     \ param 113, OPn kb level scaling break point, 0-99
\ ;
: rndkb ( -- c c c c c c ) \ normalised to max values
   8 CHOOSE          \ param 118, OPn kb rate scaling, 0-7
   4 CHOOSE          \ param 117, OPn kb level scaling R curve, 0-3
   4 CHOOSE          \ param 116, OPn kb level scaling L curve, 0-3
   10                \ param 115, OPn kb level scaling R depth, 0-99
   10                \ param 114, OPn kb level scaling L depth, 0-99
   choose.norm50   \ param 113, OPn kb level scaling break point, 0-99
;

CREATE opvolumes 6 ALLOT
: randomvolumes! ( -- )    \ pregenerate an array of random volumes
   6 0
   DO
      90 10 CHOOSE +    \ param 121, OPn output level, 0-99, at least 90
      opvolumes I + C!
   LOOP
;

\ insert a 99 at a rand position in opvolumes
: injectfullvol!  ( -- )   99 opvolumes 6 CHOOSE + C! ;
: prepvolumes     ( -- )   randomvolumes! injectfullvol! injectfullvol! ;
\ DEBUG: put the array bytes on the stack
: randomvolumes@  ( -- n n n n n n )   6 0 DO opvolumes I + C@ LOOP ;

\ params 121, 120, 119, OP misc amplitude stuff
: rndamp ( -- c c c )
   opvolumes I + C@  \ param 121, fetch OPn volume from pre-generated array 
   8   CHOOSE     \ param 120, OPn key velocity sensitivity, 0-7
   4   CHOOSE     \ param 119, OPn amp modulation sensitivity, 0-3
;   

\ params 125, 124, 123, 122, OP osc parameters
: rndosc ( -- c c c c )
   choose.norm7   \ param 125, OPn osc detune, 0-14
   100 CHOOSE     \ param 124, OPn osc frequency fine, 0-99
   choose.expo1   \ param 123, OPn osc frequency coarse, 0-31
   2 CHOOSE 2 CHOOSE *  \ param 122, OPn osc mode, 0-1
;  

\ params 126-133, pitch EG rates and levels, 0-99
: rndpitcheg ( -- c c c c c c c c)
   8 0 DO         \ 8 bytes
      50  \ pitch EG R1, R2, R3, R4, L1, L2, L3, L4
   LOOP
;

\ params 136, 135, 134, FM settings
VARIABLE fm-variation
0 fm-variation !

\ four algo families and several variations
\ - stacks:    1,2   | 3,4    | 5,6
\ - branches:  7,8,9 | 10,11  | 12,13     | 14,15  | 16,17,18
\ - roots:     19    | 20     | 21,22,23  | 24,25
\ - mixed:     26,27 | 28     | 29,30     | 31,32 

: pickfmalgo ( variation -- fm-algo )
   CASE
      1  OF 2 CHOOSE 0  + ENDOF     \ 0,1      (stacking family) 
      2  OF 2 CHOOSE 2  + ENDOF     \ 2,3      (stacking family) 
      3  OF 2 CHOOSE 4  + ENDOF     \ 4,5      (stacking family) 
      4  OF 3 CHOOSE 6  + ENDOF     \ 6,7,8    (branching family) 
      5  OF 2 CHOOSE 9  + ENDOF     \ 9,10     (branching family) 
      6  OF 2 CHOOSE 11 + ENDOF     \ 11,12    (branching family) 
      7  OF 2 CHOOSE 13 + ENDOF     \ 13,14    (branching family) 
      8  OF 3 CHOOSE 15 + ENDOF     \ 15,16,17 (branching family) 
      9  OF 1 CHOOSE 18 + ENDOF     \ 18       (rhizome family) 
      10 OF 1 CHOOSE 19 + ENDOF     \ 19       (rhizome family) 
      11 OF 3 CHOOSE 20 + ENDOF     \ 20,21,22 (rhizome family) 
      12 OF 2 CHOOSE 23 + ENDOF     \ 23,24    (rhizome family)    
      13 OF 2 CHOOSE 25 + ENDOF     \ 25,26    (mixed family) 
      14 OF 1 CHOOSE 27 + ENDOF     \ 27       (mixed family) 
      15 OF 2 CHOOSE 28 + ENDOF     \ 28,29    (mixed family) 
      16 OF 2 CHOOSE 30 + ENDOF     \ 30,31    (mixed family) 
   ENDCASE
;

: rndfm ( -- c c c )
   2  CHOOSE                     \ param 136, osc key sync, 0-1
   8  CHOOSE                     \ param 135, feedback, 0-7
   fm-variation @ pickfmalgo     \ param 134, FM algo select \o/, 0-31
;
 
\ params 143, 142, 141, 140, 139, 138, 137, LFO settings
: rndlfo ( -- c c c c c c c )
   8   CHOOSE     \ param 143, LFO pitch mod sensitivity, 0-7
   6   CHOOSE     \ param 142, LFO waveform, 0-5
   2   CHOOSE     \ param 141, LFO key sync, 0-1
   choose.expo0   \ param 140, LFO amp mod depth, 0-99
   choose.expo0   \ param 139, LFO pitch mod depth, 0-99
   100 CHOOSE     \ param 138, LFO delay, 0-99
   choose.norm50  \ param 137, LFO speed, 0-99
;

\ param 144, transpose, 0-48
\ : rndtranspose ( -- c )  49 CHOOSE ;
: rndtranspose ( -- c )  24 ;


: rndname ( -- c c c c c c c c c c )   \ preset name, params 145-154
   10 0 DO                             \ 10 bytes
      128 CHOOSE                       \ 7bit ASCII, 0-127
   LOOP
;

: rndparent ( -- n...n ) \ generate 155 genes of one potential parent
   prepvolumes    \ prepare the OP volumes array
   rndname
   rndtranspose
   rndlfo
   rndfm
   rndpitcheg
   6 0            \ 6 OPs per preset
   DO
      rndosc
      rndamp
      rndkb
      rndlvl
      rndrate
   LOOP
;

: rndparents ( -- n...n )  32 0 DO rndparent LOOP ;


