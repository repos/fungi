\ some precalc'ed LUTs for nicer randoms

ANEW TASK-precalc

CREATE normal7
0 C, 2 C, 2 C, 2 C, 3 C, 3 C, 3 C, 3 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C,
4 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 6 C, 6 C, 6 C,
6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 7 C, 7 C, 7 C,
7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 8 C, 8 C,
8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 9 C, 9 C, 9 C,
9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 10 C, 10 C, 10 C, 10 C, 10 C,
10 C, 10 C, 10 C, 11 C, 11 C, 11 C, 11 C, 12 C, 12 C, 12 C, 14 C, 

: choose.norm7    ( -- C )    normal7 100 CHOOSE + C@ ;

CREATE normal50
0 C, 7 C, 10 C, 13 C, 14 C, 16 C, 17 C, 18 C, 19 C, 20 C, 21 C, 21 C, 22 C,
23 C, 23 C, 24 C, 24 C, 25 C, 25 C, 26 C, 26 C, 27 C, 27 C, 28 C, 28 C,
28 C, 29 C, 29 C, 29 C, 30 C, 30 C, 30 C, 31 C, 31 C, 31 C, 32 C, 32 C,
32 C, 33 C, 33 C, 33 C, 33 C, 34 C, 34 C, 34 C, 34 C, 35 C, 35 C, 35 C,
35 C, 36 C, 36 C, 36 C, 36 C, 37 C, 37 C, 37 C, 37 C, 37 C, 38 C, 38 C,
38 C, 38 C, 39 C, 39 C, 39 C, 39 C, 39 C, 40 C, 40 C, 40 C, 40 C, 40 C,
41 C, 41 C, 41 C, 41 C, 41 C, 41 C, 42 C, 42 C, 42 C, 42 C, 42 C, 43 C,
43 C, 43 C, 43 C, 43 C, 43 C, 44 C, 44 C, 44 C, 44 C, 44 C, 45 C, 45 C,
45 C, 45 C, 45 C, 45 C, 46 C, 46 C, 46 C, 46 C, 46 C, 46 C, 47 C, 47 C,
47 C, 47 C, 47 C, 47 C, 48 C, 48 C, 48 C, 48 C, 48 C, 48 C, 49 C, 49 C,
49 C, 49 C, 49 C, 49 C, 50 C, 50 C, 50 C, 50 C, 50 C, 50 C, 51 C, 51 C,
51 C, 51 C, 51 C, 51 C, 52 C, 52 C, 52 C, 52 C, 52 C, 52 C, 53 C, 53 C,
53 C, 53 C, 53 C, 53 C, 54 C, 54 C, 54 C, 54 C, 54 C, 54 C, 55 C, 55 C,
55 C, 55 C, 55 C, 55 C, 56 C, 56 C, 56 C, 56 C, 56 C, 56 C, 57 C, 57 C,
57 C, 57 C, 57 C, 58 C, 58 C, 58 C, 58 C, 58 C, 58 C, 59 C, 59 C, 59 C,
59 C, 59 C, 60 C, 60 C, 60 C, 60 C, 60 C, 61 C, 61 C, 61 C, 61 C, 61 C,
62 C, 62 C, 62 C, 62 C, 62 C, 63 C, 63 C, 63 C, 63 C, 64 C, 64 C, 64 C,
64 C, 65 C, 65 C, 65 C, 65 C, 65 C, 66 C, 66 C, 66 C, 67 C, 67 C, 67 C,
67 C, 68 C, 68 C, 68 C, 69 C, 69 C, 69 C, 69 C, 70 C, 70 C, 71 C, 71 C,
71 C, 72 C, 72 C, 72 C, 73 C, 73 C, 74 C, 74 C, 75 C, 75 C, 75 C, 76 C,
77 C, 77 C, 78 C, 79 C, 79 C, 80 C, 81 C, 82 C, 83 C, 84 C, 86 C, 87 C,
89 C, 93 C, 99 C, 

: choose.norm50   ( -- C )    normal50 256 CHOOSE + C@ ;

CREATE exponential0
0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C,
1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C,
2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C,
3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C,
4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C,
5 C, 5 C, 5 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C,
7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 8 C, 8 C, 8 C, 8 C, 8 C,
8 C, 8 C, 8 C, 8 C, 8 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C,
10 C, 10 C, 10 C, 10 C, 10 C, 10 C, 10 C, 10 C, 10 C, 11 C, 11 C, 11 C,
11 C, 11 C, 11 C, 11 C, 11 C, 12 C, 12 C, 12 C, 12 C, 12 C, 12 C, 12 C,
13 C, 13 C, 13 C, 13 C, 13 C, 13 C, 13 C, 13 C, 14 C, 14 C, 14 C, 14 C,
14 C, 14 C, 15 C, 15 C, 15 C, 15 C, 15 C, 15 C, 15 C, 16 C, 16 C, 16 C,
16 C, 16 C, 17 C, 17 C, 17 C, 17 C, 17 C, 17 C, 18 C, 18 C, 18 C, 18 C,
18 C, 19 C, 19 C, 19 C, 19 C, 19 C, 20 C, 20 C, 20 C, 20 C, 21 C, 21 C,
21 C, 21 C, 21 C, 22 C, 22 C, 22 C, 22 C, 23 C, 23 C, 23 C, 24 C, 24 C,
24 C, 24 C, 25 C, 25 C, 25 C, 26 C, 26 C, 26 C, 27 C, 27 C, 27 C, 28 C,
28 C, 29 C, 29 C, 29 C, 30 C, 30 C, 31 C, 31 C, 32 C, 32 C, 33 C, 33 C,
34 C, 34 C, 35 C, 35 C, 36 C, 37 C, 37 C, 38 C, 39 C, 39 C, 40 C, 41 C,
42 C, 43 C, 44 C, 45 C, 47 C, 48 C, 49 C, 51 C, 53 C, 55 C, 58 C, 61 C,
65 C, 70 C, 78 C, 99 C, 

: choose.expo0 ( -- C )  exponential0 256 CHOOSE + C@ ;

CREATE exponential1
0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C,
0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 0 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C,
1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C,
1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 1 C,
1 C, 1 C, 1 C, 1 C, 1 C, 1 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C,
2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C,
2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 2 C, 3 C, 3 C, 3 C, 3 C,
3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C,
3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 3 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C,
4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C, 4 C,
4 C, 4 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 5 C,
5 C, 5 C, 5 C, 5 C, 5 C, 5 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 6 C,
6 C, 6 C, 6 C, 6 C, 6 C, 6 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C, 7 C,
7 C, 7 C, 7 C, 7 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 8 C, 9 C,
9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 9 C, 10 C, 10 C, 10 C, 10 C, 10 C,
10 C, 10 C, 11 C, 11 C, 11 C, 11 C, 11 C, 12 C, 12 C, 12 C, 12 C, 12 C,
13 C, 13 C, 13 C, 13 C, 14 C, 14 C, 14 C, 15 C, 15 C, 16 C, 16 C, 17 C,
17 C, 18 C, 18 C, 19 C, 20 C, 21 C, 23 C, 26 C, 31 C, 

: choose.expo1 ( -- C )  exponential1 256 CHOOSE + C@ ;
