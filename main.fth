\ FUNG!

\    ,------------.
\    |  DEFAULTS  |
\    `------------'
\

ANEW TASK-main

VARIABLE current-preset
1 current-preset !

\ Note: MIDI.PRESET only works with the TX7 when:
\ - DATA.ENTRY.VOL is ON
\ - the TX7 is in CMB mode
\ why? we'll probably never know...
\ but most likely Yamaha deviating from MIDI conventions

1 MIDI.CHANNEL!                  \ send/receive on channel 1
current-preset @ MIDI.PRESET     \ load preset 1


INCLUDE 0xA:fungi/precalc.fth



\    ,---------.
\    |  UT!LS  |
\    `---------'

: 3dup    ( n1 n2 n3 -- n1 n2 n3 n1 n2 n3 )  DUP 2OVER ROT ;
: hopa    ( n1 n2 n3 -- n3 n1 n2 n3 )  -ROT 3dup 2DROP ;
: pickone ( n n -- n )  2 CHOOSE 0= IF SWAP THEN DROP ;
: 2hex  ( n -- )  HEX . DECIMAL ;
: 2bin  ( n -- )  BINARY . DECIMAL ;


\    ,--------------.
\    |  POPULATION  |
\    `--------------'

CREATE parents    155 32 * ALLOT   \ 32 presets with 155 voice parameters
CREATE children   155 32 * ALLOT

: gene!  ( value parameter population preset -- )  155 * + + C! ;
: gene@  ( parameter population preset -- value )  155 * + + C@ ;  
: pgene! ( value parameter preset -- )    parents SWAP gene! ;
: pgene@ ( parameter preset -- value )    parents SWAP gene@ ;
: cgene! ( value parameter preset -- )    children SWAP gene! ;
: cgene@ ( parameter preset -- value )    children SWAP gene@ ;

: pgenes! ( n...n preset -- )  155 0 DO I SWAP hopa pgene! LOOP DROP ;
: parents! ( n...n -- )  32 0 DO I pgenes! LOOP ;

\ DEBUG: dump values of given parameter for each parent
: showpgene@  ( parameter -- n...n )  32 0 DO DUP I pgene@ . LOOP DROP ;

INCLUDE 0xA:fungi/rndfungi.fth

\    ,--------------------.
\    |  M!D! SYSEX STUFF  |
\    `--------------------'
\
\     Changes to a preset can be made in 3 ways:
\      - one parameter change for currently active preset
\      - every parameter change (same as if new preset basically)
\      - bulk upload of 32 whole new presets
\     Only the last option writes the changes to the TX7 memory.
\     The first twos are "performance" changes only, they will disappear
\     once a new preset is selected and/or if the synth is turned off.
\     Whyyyyyyyyyyy.....

HEX

: tx7.start.sysex ( -- )
   MIDI.START.SYSEX
   43 MIDI.XMIT
;

: tx7.end.sysex ( -- )
   MIDI.END.SYSEX
   MIDI.FLUSH           \ HMSL doc recommends to flush the buffer
;

\ parameter changes (temporary)
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 10  sub-status (1) and channel number (0, ie MIDI channel 1)
\ **  parameter group
\ **  parameter number
\ **  data byte
\ F7  status byte - end sysex

\ tx7.sendparam is a handy word to quickly change any parameter live
\ of the currently active preset. See section L!VE PARAMETER CTRL for
\ some examples.

: tx7.sendparam ( group parameter data -- ) 
   tx7.start.sysex
   10 MIDI.XMIT
   SWAP ROT
   MIDI.XMIT MIDI.XMIT MIDI.XMIT
   tx7.end.sysex
;

DECIMAL


\ The following section is ridiculous, yes, the older implementation
\ was *much* more compact but also *way* less readable and maintainable, so
\ in the new version I decided to KISS to avoid pointless stack gymnastics
\ or the use of variables and counters inside double loops

\ OP6
: bko6er1 ( preset -- )    0   SWAP pgene@ ;    \ 0   (bulk)
: bko6er2 ( preset -- )    1   SWAP pgene@ ;    \ 1   (bulk)
: bko6er3 ( preset -- )    2   SWAP pgene@ ;    \ 2   (bulk)
: bko6er4 ( preset -- )    3   SWAP pgene@ ;    \ 3   (bulk)
: bko6el1 ( preset -- )    4   SWAP pgene@ ;    \ 4   (bulk)
: bko6el2 ( preset -- )    5   SWAP pgene@ ;    \ 5   (bulk)
: bko6el3 ( preset -- )    6   SWAP pgene@ ;    \ 6   (bulk)
: bko6el4 ( preset -- )    7   SWAP pgene@ ;    \ 7   (bulk)
: bko6sbp ( preset -- )    8   SWAP pgene@ ;    \ 8   (bulk)
: bko6sld ( preset -- )    9   SWAP pgene@ ;    \ 9   (bulk)
: bko6srd ( preset -- )    10  SWAP pgene@ ;    \ 10  (bulk)

: bko6rlc ( preset -- )                         \ 11  (bulk)
   DUP 
   11 SWAP pgene@ SWAP
   12 SWAP pgene@ 2 << +
;

: bko6drs ( preset -- )                         \ 12  (bulk)
   DUP
   13 SWAP pgene@ SWAP
   20 SWAP pgene@ 3 << +
;

: bko6vms ( preset -- )                         \ 13  (bulk)
   DUP
   14 SWAP pgene@ SWAP
   15 SWAP pgene@ 2 << +
;

: bko6vol ( preset -- )    16  SWAP pgene@ ;    \ 14  (bulk)

: bko6fcm ( preset -- )                         \ 15  (bulk)
   DUP
   17 SWAP pgene@ SWAP
   18 SWAP pgene@ 1 << +
;

: bko6ffn ( preset -- )    19  SWAP pgene@ ;    \ 16  (bulk)

\ OP5
: bko5er1 ( preset -- )    21  SWAP pgene@ ;    \ 17  (bulk)
: bko5er2 ( preset -- )    22  SWAP pgene@ ;    \ 18  (bulk)
: bko5er3 ( preset -- )    23  SWAP pgene@ ;    \ 19  (bulk)
: bko5er4 ( preset -- )    24  SWAP pgene@ ;    \ 20  (bulk)
: bko5el1 ( preset -- )    25  SWAP pgene@ ;    \ 21  (bulk)
: bko5el2 ( preset -- )    26  SWAP pgene@ ;    \ 22  (bulk)
: bko5el3 ( preset -- )    27  SWAP pgene@ ;    \ 23  (bulk)
: bko5el4 ( preset -- )    28  SWAP pgene@ ;    \ 24  (bulk)
: bko5sbp ( preset -- )    29  SWAP pgene@ ;    \ 25  (bulk)
: bko5sld ( preset -- )    30  SWAP pgene@ ;    \ 26  (bulk)
: bko5srd ( preset -- )    31  SWAP pgene@ ;    \ 27  (bulk)

: bko5rlc ( preset -- )                         \ 28  (bulk)
   DUP 
   32 SWAP pgene@ SWAP
   33 SWAP pgene@ 2 << +
;

: bko5drs ( preset -- )                         \ 29  (bulk)
   DUP
   34 SWAP pgene@ SWAP
   41 SWAP pgene@ 3 << +
;

: bko5vms ( preset -- )                         \ 30  (bulk)
   DUP
   35 SWAP pgene@ SWAP
   36 SWAP pgene@ 2 << +
;

: bko5vol ( preset -- )    37  SWAP pgene@ ;    \ 31  (bulk)

: bko5fcm ( preset -- )                         \ 32  (bulk)
   DUP
   38 SWAP pgene@ SWAP
   39 SWAP pgene@ 1 << +
;

: bko5ffn ( preset -- )    40  SWAP pgene@ ;    \ 33  (bulk)

\ OP4
: bko4er1 ( preset -- )    42  SWAP pgene@ ;    \ 34  (bulk)
: bko4er2 ( preset -- )    43  SWAP pgene@ ;    \ 35  (bulk)
: bko4er3 ( preset -- )    44  SWAP pgene@ ;    \ 36  (bulk)
: bko4er4 ( preset -- )    45  SWAP pgene@ ;    \ 37  (bulk)
: bko4el1 ( preset -- )    46  SWAP pgene@ ;    \ 38  (bulk)
: bko4el2 ( preset -- )    47  SWAP pgene@ ;    \ 39  (bulk)
: bko4el3 ( preset -- )    48  SWAP pgene@ ;    \ 40  (bulk)
: bko4el4 ( preset -- )    49  SWAP pgene@ ;    \ 41  (bulk)
: bko4sbp ( preset -- )    50  SWAP pgene@ ;    \ 42  (bulk)
: bko4sld ( preset -- )    51  SWAP pgene@ ;    \ 43  (bulk)
: bko4srd ( preset -- )    52  SWAP pgene@ ;    \ 44  (bulk)

: bko4rlc ( preset -- )                         \ 45  (bulk)
   DUP 
   53 SWAP pgene@ SWAP
   54 SWAP pgene@ 2 << +
;

: bko4drs ( preset -- )                         \ 46  (bulk)
   DUP
   55 SWAP pgene@ SWAP
   62 SWAP pgene@ 3 << +
;

: bko4vms ( preset -- )                         \ 47  (bulk)
   DUP
   56 SWAP pgene@ SWAP
   57 SWAP pgene@ 2 << +
;

: bko4vol ( preset -- )    58  SWAP pgene@ ;    \ 48  (bulk)

: bko4fcm ( preset -- )                         \ 49  (bulk)
   DUP
   59 SWAP pgene@ SWAP
   60 SWAP pgene@ 1 << +
;

: bko4ffn ( preset -- )    61  SWAP pgene@ ;    \ 50  (bulk)

\ OP3
: bko3er1 ( preset -- )    63  SWAP pgene@ ;    \ 51  (bulk)
: bko3er2 ( preset -- )    64  SWAP pgene@ ;    \ 52  (bulk)
: bko3er3 ( preset -- )    65  SWAP pgene@ ;    \ 53  (bulk)
: bko3er4 ( preset -- )    66  SWAP pgene@ ;    \ 54  (bulk)
: bko3el1 ( preset -- )    67  SWAP pgene@ ;    \ 55  (bulk)
: bko3el2 ( preset -- )    68  SWAP pgene@ ;    \ 56  (bulk)
: bko3el3 ( preset -- )    69  SWAP pgene@ ;    \ 57  (bulk)
: bko3el4 ( preset -- )    70  SWAP pgene@ ;    \ 58  (bulk)
: bko3sbp ( preset -- )    71  SWAP pgene@ ;    \ 59  (bulk)
: bko3sld ( preset -- )    72  SWAP pgene@ ;    \ 60  (bulk)
: bko3srd ( preset -- )    73  SWAP pgene@ ;    \ 61  (bulk)

: bko3rlc ( preset -- )                         \ 62  (bulk)
   DUP 
   74 SWAP pgene@ SWAP
   75 SWAP pgene@ 2 << +
;

: bko3drs ( preset -- )                         \ 63  (bulk)
   DUP
   76 SWAP pgene@ SWAP
   83 SWAP pgene@ 3 << +
;

: bko3vms ( preset -- )                         \ 64  (bulk)
   DUP
   77 SWAP pgene@ SWAP
   78 SWAP pgene@ 2 << +
;

: bko3vol ( preset -- )    79  SWAP pgene@ ;    \ 65  (bulk)

: bko3fcm ( preset -- )                         \ 66  (bulk)
   DUP
   80 SWAP pgene@ SWAP
   81 SWAP pgene@ 1 << +
;

: bko3ffn ( preset -- )    82  SWAP pgene@ ;    \ 67  (bulk)

\ OP2
: bko2er1 ( preset -- )    84  SWAP pgene@ ;    \ 68  (bulk)
: bko2er2 ( preset -- )    85  SWAP pgene@ ;    \ 69  (bulk)
: bko2er3 ( preset -- )    86  SWAP pgene@ ;    \ 70  (bulk)
: bko2er4 ( preset -- )    87  SWAP pgene@ ;    \ 71  (bulk)
: bko2el1 ( preset -- )    88  SWAP pgene@ ;    \ 72  (bulk)
: bko2el2 ( preset -- )    89  SWAP pgene@ ;    \ 73  (bulk)
: bko2el3 ( preset -- )    90  SWAP pgene@ ;    \ 74  (bulk)
: bko2el4 ( preset -- )    91  SWAP pgene@ ;    \ 75  (bulk)
: bko2sbp ( preset -- )    92  SWAP pgene@ ;    \ 76  (bulk)
: bko2sld ( preset -- )    93  SWAP pgene@ ;    \ 77  (bulk)
: bko2srd ( preset -- )    94  SWAP pgene@ ;    \ 78  (bulk)

: bko2rlc ( preset -- )                         \ 79  (bulk)
   DUP 
   95 SWAP pgene@ SWAP
   96 SWAP pgene@ 2 << +
;

: bko2drs ( preset -- )                         \ 80  (bulk)
   DUP
   97  SWAP pgene@ SWAP
   104 SWAP pgene@ 3 << +
;

: bko2vms ( preset -- )                         \ 81  (bulk)
   DUP
   98 SWAP pgene@ SWAP
   99 SWAP pgene@ 2 << +
;

: bko2vol ( preset -- )    100 SWAP pgene@ ;    \ 82  (bulk)

: bko2fcm ( preset -- )                         \ 83  (bulk)
   DUP
   101 SWAP pgene@ SWAP
   102 SWAP pgene@ 1 << +
;

: bko2ffn ( preset -- )    103 SWAP pgene@ ;    \ 84  (bulk)

\ OP1
: bko1er1 ( preset -- )    105 SWAP pgene@ ;    \ 85  (bulk)
: bko1er2 ( preset -- )    106 SWAP pgene@ ;    \ 86  (bulk)
: bko1er3 ( preset -- )    107 SWAP pgene@ ;    \ 87  (bulk)
: bko1er4 ( preset -- )    108 SWAP pgene@ ;    \ 88  (bulk)
: bko1el1 ( preset -- )    109 SWAP pgene@ ;    \ 89  (bulk)
: bko1el2 ( preset -- )    110 SWAP pgene@ ;    \ 90  (bulk)
: bko1el3 ( preset -- )    111 SWAP pgene@ ;    \ 91  (bulk)
: bko1el4 ( preset -- )    112 SWAP pgene@ ;    \ 92  (bulk)
: bko1sbp ( preset -- )    113 SWAP pgene@ ;    \ 93  (bulk)
: bko1sld ( preset -- )    114 SWAP pgene@ ;    \ 94  (bulk)
: bko1srd ( preset -- )    115 SWAP pgene@ ;    \ 95  (bulk)

: bko1rlc ( preset -- )                         \ 96  (bulk)
   DUP 
   116 SWAP pgene@ SWAP
   117 SWAP pgene@ 2 << +
;

: bko1drs ( preset -- )                         \ 97  (bulk)
   DUP
   118 SWAP pgene@ SWAP
   125 SWAP pgene@ 3 << +
;

: bko1vms ( preset -- )                         \ 98  (bulk)
   DUP
   119 SWAP pgene@ SWAP
   120 SWAP pgene@ 2 << +
;

: bko1vol ( preset -- )    121 SWAP pgene@ ;    \ 99  (bulk)

: bko1fcm ( preset -- )                         \ 100 (bulk)
   DUP
   122 SWAP pgene@ SWAP
   123 SWAP pgene@ 1 << +
;

: bko1ffn ( preset -- )    124 SWAP pgene@ ;    \ 101 (bulk)

\ the rest...
: bkpegr1 ( preset -- )    126 SWAP pgene@ ;    \ 102 (bulk)
: bkpegr2 ( preset -- )    127 SWAP pgene@ ;    \ 103 (bulk)
: bkpegr3 ( preset -- )    128 SWAP pgene@ ;    \ 104 (bulk)
: bkpegr4 ( preset -- )    129 SWAP pgene@ ;    \ 105 (bulk)
: bkpegl1 ( preset -- )    130 SWAP pgene@ ;    \ 106 (bulk)
: bkpegl2 ( preset -- )    131 SWAP pgene@ ;    \ 107 (bulk)
: bkpegl3 ( preset -- )    132 SWAP pgene@ ;    \ 108 (bulk)
: bkpegl4 ( preset -- )    133 SWAP pgene@ ;    \ 109 (bulk)
: bkalgos ( preset -- )    134 SWAP pgene@ ;    \ 110 (bulk)

: bkoscsf ( preset -- )                         \ 111 (bulk)
   DUP
   135 SWAP pgene@ SWAP
   136 SWAP pgene@ 3 << +
;

: bklfosp ( preset -- )    137 SWAP pgene@ ;    \ 112 (bulk)
: bklfodl ( preset -- )    138 SWAP pgene@ ;    \ 113 (bulk)
: bklfopd ( preset -- )    139 SWAP pgene@ ;    \ 114 (bulk)
: bklfoad ( preset -- )    140 SWAP pgene@ ;    \ 115 (bulk)

: bklfmws ( preset -- )                         \ 116 (bulk)
   DUP DUP
   141 SWAP pgene@ SWAP
   142 SWAP pgene@ 1 << + SWAP
   143 SWAP pgene@ 4 << +
;

: bktrans ( preset -- )    144 SWAP pgene@ ;    \ 117 (bulk)
: bkvnam0 ( preset -- )    145 SWAP pgene@ ;    \ 118 (bulk)
: bkvnam1 ( preset -- )    146 SWAP pgene@ ;    \ 119 (bulk)
: bkvnam2 ( preset -- )    147 SWAP pgene@ ;    \ 120 (bulk)
: bkvnam3 ( preset -- )    148 SWAP pgene@ ;    \ 121 (bulk)
: bkvnam4 ( preset -- )    149 SWAP pgene@ ;    \ 122 (bulk)
: bkvnam5 ( preset -- )    150 SWAP pgene@ ;    \ 123 (bulk)
: bkvnam6 ( preset -- )    151 SWAP pgene@ ;    \ 124 (bulk)
: bkvnam7 ( preset -- )    152 SWAP pgene@ ;    \ 125 (bulk)
: bkvnam8 ( preset -- )    153 SWAP pgene@ ;    \ 126 (bulk)
: bkvnam9 ( preset -- )    154 SWAP pgene@ ;    \ 127 (bulk)

\ bulk data for 32 presets (saved in memory)
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 00  sub-status (0) and channel number (0, ie MIDI channel 1)
\ 09  format number (9, ie 32 presets)
\ 20  byte count MS byte
\ 00  byte count LS byte (4096, 32 presets)
\ **  data byte 1
\ **  ...
\ **  data byte 4096
\ **  checksum (masked 2's complement of sum of 4096 bytes)
\ F7  status byte - end sysex
\ 

HEX

\ NOTE: slightly delayed MIDI.XMIT because otherwise it does not work.
\ unsure why, TX7 can't cope or is it my midihub in between??
: tx7.bk.xmit  ( chk xt -- chk )  I SWAP EXECUTE DUP MIDI.XMIT 2 MSEC + ;

\ it's aint much, but it's honest work :)
: tx7.bulk.upload ( -- )
   tx7.start.sysex 
   00 MIDI.XMIT 
   09 MIDI.XMIT
   20 MIDI.XMIT
   00 MIDI.XMIT
   0                                   \ Yamaha checksum algo 1/2
   20 0 DO                             \ 32 presets, 20 in HEX
      ' bko6er1 tx7.bk.xmit
      ' bko6er2 tx7.bk.xmit 
      ' bko6er3 tx7.bk.xmit 
      ' bko6er4 tx7.bk.xmit 
      ' bko6el1 tx7.bk.xmit 
      ' bko6el2 tx7.bk.xmit 
      ' bko6el3 tx7.bk.xmit 
      ' bko6el4 tx7.bk.xmit 
      ' bko6sbp tx7.bk.xmit 
      ' bko6sld tx7.bk.xmit 
      ' bko6srd tx7.bk.xmit 
      ' bko6rlc tx7.bk.xmit 
      ' bko6drs tx7.bk.xmit 
      ' bko6vms tx7.bk.xmit 
      ' bko6vol tx7.bk.xmit 
      ' bko6fcm tx7.bk.xmit 
      ' bko6ffn tx7.bk.xmit 
      ' bko5er1 tx7.bk.xmit 
      ' bko5er2 tx7.bk.xmit 
      ' bko5er3 tx7.bk.xmit 
      ' bko5er4 tx7.bk.xmit 
      ' bko5el1 tx7.bk.xmit 
      ' bko5el2 tx7.bk.xmit 
      ' bko5el3 tx7.bk.xmit 
      ' bko5el4 tx7.bk.xmit 
      ' bko5sbp tx7.bk.xmit 
      ' bko5sld tx7.bk.xmit 
      ' bko5srd tx7.bk.xmit 
      ' bko5rlc tx7.bk.xmit 
      ' bko5drs tx7.bk.xmit 
      ' bko5vms tx7.bk.xmit 
      ' bko5vol tx7.bk.xmit 
      ' bko5fcm tx7.bk.xmit 
      ' bko5ffn tx7.bk.xmit 
      ' bko4er1 tx7.bk.xmit 
      ' bko4er2 tx7.bk.xmit 
      ' bko4er3 tx7.bk.xmit 
      ' bko4er4 tx7.bk.xmit 
      ' bko4el1 tx7.bk.xmit 
      ' bko4el2 tx7.bk.xmit 
      ' bko4el3 tx7.bk.xmit 
      ' bko4el4 tx7.bk.xmit 
      ' bko4sbp tx7.bk.xmit 
      ' bko4sld tx7.bk.xmit 
      ' bko4srd tx7.bk.xmit 
      ' bko4rlc tx7.bk.xmit 
      ' bko4drs tx7.bk.xmit 
      ' bko4vms tx7.bk.xmit 
      ' bko4vol tx7.bk.xmit 
      ' bko4fcm tx7.bk.xmit 
      ' bko4ffn tx7.bk.xmit 
      ' bko3er1 tx7.bk.xmit 
      ' bko3er2 tx7.bk.xmit 
      ' bko3er3 tx7.bk.xmit 
      ' bko3er4 tx7.bk.xmit 
      ' bko3el1 tx7.bk.xmit 
      ' bko3el2 tx7.bk.xmit 
      ' bko3el3 tx7.bk.xmit 
      ' bko3el4 tx7.bk.xmit 
      ' bko3sbp tx7.bk.xmit 
      ' bko3sld tx7.bk.xmit 
      ' bko3srd tx7.bk.xmit 
      ' bko3rlc tx7.bk.xmit 
      ' bko3drs tx7.bk.xmit 
      ' bko3vms tx7.bk.xmit 
      ' bko3vol tx7.bk.xmit 
      ' bko3fcm tx7.bk.xmit 
      ' bko3ffn tx7.bk.xmit 
      ' bko2er1 tx7.bk.xmit 
      ' bko2er2 tx7.bk.xmit 
      ' bko2er3 tx7.bk.xmit 
      ' bko2er4 tx7.bk.xmit 
      ' bko2el1 tx7.bk.xmit 
      ' bko2el2 tx7.bk.xmit 
      ' bko2el3 tx7.bk.xmit 
      ' bko2el4 tx7.bk.xmit 
      ' bko2sbp tx7.bk.xmit 
      ' bko2sld tx7.bk.xmit 
      ' bko2srd tx7.bk.xmit 
      ' bko2rlc tx7.bk.xmit 
      ' bko2drs tx7.bk.xmit 
      ' bko2vms tx7.bk.xmit 
      ' bko2vol tx7.bk.xmit 
      ' bko2fcm tx7.bk.xmit 
      ' bko2ffn tx7.bk.xmit 
      ' bko1er1 tx7.bk.xmit 
      ' bko1er2 tx7.bk.xmit 
      ' bko1er3 tx7.bk.xmit 
      ' bko1er4 tx7.bk.xmit 
      ' bko1el1 tx7.bk.xmit 
      ' bko1el2 tx7.bk.xmit 
      ' bko1el3 tx7.bk.xmit 
      ' bko1el4 tx7.bk.xmit 
      ' bko1sbp tx7.bk.xmit 
      ' bko1sld tx7.bk.xmit 
      ' bko1srd tx7.bk.xmit 
      ' bko1rlc tx7.bk.xmit 
      ' bko1drs tx7.bk.xmit 
      ' bko1vms tx7.bk.xmit 
      ' bko1vol tx7.bk.xmit 
      ' bko1fcm tx7.bk.xmit 
      ' bko1ffn tx7.bk.xmit 
      ' bkpegr1 tx7.bk.xmit 
      ' bkpegr2 tx7.bk.xmit 
      ' bkpegr3 tx7.bk.xmit 
      ' bkpegr4 tx7.bk.xmit 
      ' bkpegl1 tx7.bk.xmit 
      ' bkpegl2 tx7.bk.xmit 
      ' bkpegl3 tx7.bk.xmit 
      ' bkpegl4 tx7.bk.xmit 
      ' bkalgos tx7.bk.xmit 
      ' bkoscsf tx7.bk.xmit 
      ' bklfosp tx7.bk.xmit 
      ' bklfodl tx7.bk.xmit 
      ' bklfopd tx7.bk.xmit 
      ' bklfoad tx7.bk.xmit 
      ' bklfmws tx7.bk.xmit 
      ' bktrans tx7.bk.xmit 
      ' bkvnam0 tx7.bk.xmit 
      ' bkvnam1 tx7.bk.xmit 
      ' bkvnam2 tx7.bk.xmit 
      ' bkvnam3 tx7.bk.xmit 
      ' bkvnam4 tx7.bk.xmit 
      ' bkvnam5 tx7.bk.xmit 
      ' bkvnam6 tx7.bk.xmit 
      ' bkvnam7 tx7.bk.xmit 
      ' bkvnam8 tx7.bk.xmit 
      ' bkvnam9 tx7.bk.xmit 
   LOOP
   7F XOR 1 + 7F AND MIDI.XMIT         \ Yamaha checksum algo 2/2
   tx7.end.sysex
;

DECIMAL


\   ,--------------.
\   |  IT'S AL!VE  |
\   `--------------'
\   fitness, and gene mixing stuff goes here

100 CONSTANT maxfitvalue
CREATE fitness 32 ALLOT
CREATE pool 32 maxfitvalue * ALLOT

: resetfitness  ( -- )            fitness 32 1 FILL ;
: showfitness   ( -- array )      fitness 32 DUMP ;
: showpool      ( -- array )      pool 32 maxfitvalue * DUMP ;
: emptypool     ( -- )            pool 32 maxfitvalue * ERASE ;
: poolsize      ( -- b )          0 32 0 DO fitness I + C@ + LOOP ;
: xerox         ( b n -- b * n )  0 DO DUP LOOP DROP ;
: swimmers      ( -- b...b )      32 0 DO I fitness I + C@ xerox LOOP ;
: fillpool      ( -- )            poolsize 0 DO pool I + C! LOOP ;
: newpool       ( -- )            emptypool swimmers fillpool ;

resetfitness
emptypool

: pickparents   ( -- preset preset )        
   poolsize CHOOSE pool + C@
   poolsize CHOOSE pool + C@
   2DUP = IF 2DROP RECURSE THEN
;

: pickgene  ( preset preset parameter -- value )
   SWAP OVER
   SWAP pgene@ -ROT SWAP pgene@
   pickone
;

: makechild ( preset -- )
   pickparents
   155 0
   DO
      3DUP I pickgene I ROT cgene!
   LOOP
   3DROP
;

: makechildren ( -- )  32 0 DO I makechild LOOP ;
: nextgen      ( -- )  4960 0 DO children I + C@ parents I + C! LOOP ;


\   ,-----------------------.
\   |  L!VE PARAMETER CTRL  |
\   `-----------------------'
\   debug and helper words based on tx7.sendparam
\   it's all temporary changes and used mostly to 
\   test a few things live, or access menu settings
\   more commonly accessed from the physical interface
   
\ interface control
: tx7.individual     ( -- )  17 4 1 tx7.sendparam ;  \ IND individual mode
: tx7.combined       ( -- )  17 4 0 tx7.sendparam ;  \ CMB combined mode
: tx7.memprotect.off ( -- )  17 7 0 tx7.sendparam ;  \ disable mem protect

\ voice control
: tx7.allops.on      ( -- )  1 27 63 tx7.sendparam ; \ turn on every OPs
: tx7.allops.off     ( -- )  1 27  0 tx7.sendparam ; \ turn off every OPs

: tx7.quiet ( -- )
   tx7.allops.off
   16 0 DO 32 MIDI.NOTE 1 MSEC MIDI.LASTOFF LOOP
   tx7.allops.on
;


\   ,-------------. 
\   |  MA!N WORDS |
\   `-------------' 
\    most of the time that will be the only
\    words needed to grow and experiment with
\    new fungi

: welcome
   tx7.memprotect.off
   tx7.combined
   tx7.allops.on
   ." The following words are available: " CR
   ." randomfungi ( -- )   fill TX7 with new random parents " CR
   ." fungus+     ( -- )   next parent " CR
   ." fungus-     ( -- )   previous parent " CR
   ." fit         ( b -- ) set fitness for current parent " CR
   ." newfungi    ( -- )   fill TX7 with children " CR
;

: randomfungi ( fm-variation -- )
   1 MAX 16 MIN fm-variation !
   resetfitness
   rndparents
   parents!
   1 DUP current-preset ! MIDI.PRESET
   tx7.bulk.upload
   \ tx7.quiet          \ too slow? not needed if preset switching used?
   tx7.combined         \ NOTE: comment to DEBUG MIDI reception
;

: fungus+ ( -- )  \ load next preset
   current-preset @
   1 + 32 MIN DUP MIDI.PRESET
   current-preset !
;

: fungus- ( -- )  \ load next preset
   current-preset @
   1 - 1 MAX DUP MIDI.PRESET
   current-preset !
;


: fit    ( n -- )    current-preset @ 1 - fitness + C! ;
: nope   ( -- )      1 fit ;
: nice   ( -- )      maxfitvalue 2 / fit ;
: great  ( -- )      maxfitvalue fit ;

: newfungi ( -- )
   newpool
   makechildren
   nextgen
   tx7.bulk.upload
   tx7.combined
   resetfitness
;

welcome
