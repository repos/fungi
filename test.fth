\ temporary words to test stuff and find bugs

\ evolve the population for n iterations, always give max fitness
\ to the first preset only
: converge  ( n -- )
   1 MAX 50 MIN         \ 1-50 clipping
   1 randomfungi        \ new random population
   0 DO 
      great             \ give maximum fitness for first preset
      newfungi          \ next generation
   LOOP
;

\ quick and dirty map of XOR'ed param values across all presets to get
\ a sense of the current population diversity
: differsity ( -- )
   0 127 -DO I 0 pgene@ 1 -LOOP
   128 0 DO
      32 1 DO
         J I pgene@ XOR
      LOOP .
   LOOP
;

