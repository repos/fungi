\   ,------------.
\   |  RNDFUNGI  |
\   `------------'
\   This section takes care of creating random TX7 presets, it's not 100%
\   random though. The TX7 is designed in a way that there are quite a lot
\   of parameters combos that generate rather crappy stuff, that is when 
\   anything audible happens. There are occasional happy accidents(TM) in 
\   this vast FM territory, but such rarity can't justify having to go
\   through plethora useless presets. The cheats used to make the
\   randomness more paletable come from ancient, long forgotten TX7 sorcery,
\   combined with some personal preferences:
\   - each preset: at least one OP volume set to 99
\   - each OP: at least one EG level set to 99
\   - each OP: EG level 4 is always set to 0
\   - each OP: EG level 1 is always at least 50
\   - each OP: depth level R and L is always at least 75
\   - each OP: minimum 50 volume
\   - each OP: no keyboard break point, rate scaling, weird curve, etc


ANEW TASK-rndfungi

HEX 

\ bulk data for 32 presets (saved in memory)
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 00  sub-status (0) and channel number (0, ie MIDI channel 1)
\ 09  format number (9, ie 32 presets)
\ 20  byte count MS byte
\ 00  byte count LS byte (4096, 32 presets)
\ **  data byte 1
\ **  ...
\ **  data byte 4096
\ **  checksum (masked 2's complement of sum of 4096 bytes)
\ F7  status byte - end sysex
\ 

\ 
CREATE opvolumes 6 ALLOT
: randomvolumes! ( -- )    \ pregenerate an array of random volumes
   6 0
   DO
      32 32 CHOOSE +       \ 75-99  4B 19
      opvolumes I + C!
   LOOP
;

: injectfullvol! ( -- )      \ insert a 99 at a rand position in opvolumes
   63 opvolumes 6 CHOOSE + C!
;

: randomvolumes ( -- )
   randomvolumes!
   injectfullvol!
   injectfullvol!
;

: randomvolumes@ ( -- n n n n n n )  \ put the array bytes on the stack
   6 0
   DO
      opvolumes I + C@
   LOOP
;

\
CREATE opeglevels 4 ALLOT
: randomopeglevels! ( -- ) \ pregenerate an array of random-ish EG levels
   32 32 CHOOSE + opeglevels     C!    \ L1 is at least 50
   64 CHOOSE      opeglevels 1 + C!    \ L2 0-99
   64 CHOOSE      opeglevels 2 + C!    \ L3 0-99
   0              opeglevels 3 + C!    \ L4 is always 0
;

: injectfullopeglevel! ( -- ) \ insert a 99 at a rand position in opvolumes
   63 opeglevels 3 CHOOSE + C!   \ only first 3 bytes though
;

: randomopeglevels ( -- )
   randomopeglevels!
   injectfullopeglevel!
;

: randomopeglevels@ ( -- n n n n )  \ put the array bytes on the stack
   4 0
   DO
      opeglevels I + C@
   LOOP
;

: xmit.rndopeg ( n -- n )     \ 8 bytes of EG parameters (0-99, 64 in HEX)
   64 CHOOSE DUP MIDI.XMIT add+del        \ R1
   64 CHOOSE DUP MIDI.XMIT add+del        \ R2
   64 CHOOSE DUP MIDI.XMIT add+del        \ R3
   64 CHOOSE DUP MIDI.XMIT add+del        \ R4
   randomopeglevels
   opeglevels     C@ DUP MIDI.XMIT add+del   \ L1
   opeglevels 1 + C@ DUP MIDI.XMIT add+del   \ L2
   opeglevels 2 + C@ DUP MIDI.XMIT add+del   \ L3
   opeglevels 3 + C@ DUP MIDI.XMIT add+del   \ L4
;

: xmit.rndscbrk ( -- )     \ keyboard level scaling break point
   \ 28 14 CHOOSE + DUP MIDI.XMIT   \ 40-60, 28 + 14 in HEX
   63 DUP MIDI.XMIT                 \ 99, 63 in HEX
   add+del
;

: xmit.rnddepth ( -- )     \ keyboard level scaling depth
   \ 4B F CHOOSE + DUP MIDI.XMIT	add+del  \ left, 75-99, 64 in HEX
   \ 4B F CHOOSE + DUP MIDI.XMIT	add+del  \ right, 75-99, 64 in HEX
   63 DUP MIDI.XMIT	add+del              \ left, 99, 63 in HEX
   63 DUP MIDI.XMIT	add+del              \ right, 99, 63 in HEX
;

: xmit.rndcurve ( -- )     \ 2 combined keyboard level scale parameters
   \ 4 CHOOSE     \ left curve, 0-3, 4 in HEX               000--bb
   \ 4 CHOOSE     \ right curve, 0-3, 4 in HEX              000bb--
   3              \ left curve, 3                           000bb--
   3              \ right curve, 3                          000bb--
   shift2 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.rndtune ( -- )      \ 2 combined tuning parameters
   \ 8 CHOOSE     \ osc rate scale, 0-7, 8 in HEX           ----bbb
   7              \ osc rate scale, 7                       ----bbb
   F CHOOSE       \ osc detune, 0-14, F in HEX              bbbb---
   shift3 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.rndsense ( -- )     \ 2 combined sensitivity parameters
   4 CHOOSE    \ amp mod sensitivity, 0-3, 4 in HEX          00---bb
   8 CHOOSE    \ key vel sensitivity, 0-7, 8 in HEX          00bbb--
   shift2 bin+
   DUP MIDI.XMIT
   add+del
;

\ TODO implement several rand volume strategies
: xmit.rndvolume ( -- )     \ op output level
   \ 4B                            \ 0-99, 64 in HEX... BUT cheating a bit
   \ 19 CHOOSE + DUP MIDI.XMIT     \ to boost volume (+75/4B) for profit
   \ A CHOOSE A * 9 + DUP MIDI.XMIT
   opvolumes I + C@ DUP MIDI.XMIT
   add+del
;

: xmit.rndfreq ( -- )      \ 2 combined osc parameters
   2 CHOOSE	   \ osc mode, 0-1, 2 in HEX                     0-----b
   20 CHOOSE   \ osc coarse frequency, 0-31, 20 in HEX       0bbbbb-
   shift1 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.rndfreqfn ( -- )       \ osc fine frequency parameter
   64 CHOOSE DUP MIDI.XMIT       \ 0-99, 64 in HEX
   add+del
;

: xmit.rndpitcheg ( n -- n )
   8 0                           \ 8 bytes, 8 in HEX
   DO                            \ pitch EG R1, R2, R3, R4, L1, L2, L3, L4
      64 CHOOSE DUP MIDI.XMIT    \ 0-99, 64 in HEX
      add+del
   LOOP
;

: xmit.rndalgo ( n -- n )     \ FM algorithm \o/
   20 CHOOSE DUP MIDI.XMIT       \ 0-31, 20 in HEX
   add+del
;

: xmit.rndfbsync ( n -- n )   \ 2 combined osc parameters
   8 CHOOSE    \ feedback, 0-7, 8 in HEX                     000-bbb
   2 CHOOSE    \ osc sync, 0-1, 2 in HEX                     000b---
   shift3 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.rndlfospeed ( n -- n ) \ LFO speed
   64 CHOOSE DUP MIDI.XMIT       \ 0-99, 64 in HEX
   add+del
;

: xmit.rndlfodelay ( n -- n ) \ LFO delay
   64 CHOOSE DUP MIDI.XMIT       \ 0-99, 64 in HEX
   add+del
;

: xmit.rndlfopitch ( n -- n ) \ LFO pitch mod depth
   64 CHOOSE DUP MIDI.XMIT       \ 0-99, 64 in HEX
   add+del
;

: xmit.rndlfoamp ( n -- n )   \ LFO amp mod depth
   64 CHOOSE DUP MIDI.XMIT       \ 0-99, 64 in HEX
   add+del
;

: xmit.rndmisclfo ( n -- n )  \ 3 combined LFO parameters
   2 CHOOSE    \ LFO sync, 0-1, 2 in HEX                     ------b
   6 CHOOSE    \ LFO waveform, 0-5, 6 in HEX                 ---bbb-
   8 CHOOSE    \ LFO pitch mod sensitivity, 0-7, 8 in HEX    bbb----
   shift4 swap
   shift1 bin+ bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.rndtranspose ( n -- n )
   31 CHOOSE DUP MIDI.XMIT    \ 0-48, 31 in HEX
   add+del
;

: xmit.rndname ( n -- n )
   A 0                           \ 10 bytes, A in HEX
   DO
      80 CHOOSE DUP MIDI.XMIT    \ 7bit ASCII, 0-127, 80 in HEX
      add+del
   LOOP
;

: tx7.rndpresets ( -- )
   tx7.start.sysex
   00 MIDI.XMIT
   09 MIDI.XMIT
   20 MIDI.XMIT
   00 MIDI.XMIT
   0
   20 0           \ 32 presets, 20 in HEX
   DO
      randomvolumes!
      6 0         \ 6 ops, 6 in HEX
      DO
         xmit.rndopeg
         xmit.rndscbrk
         xmit.rnddepth
         xmit.rndcurve
         xmit.rndtune
         xmit.rndsense
         xmit.rndvolume
         xmit.rndfreq
         xmit.rndfreqfn
      LOOP
      xmit.rndpitcheg
      xmit.rndalgo
      xmit.rndfbsync
      xmit.rndlfospeed
      xmit.rndlfodelay
      xmit.rndlfopitch
      xmit.rndlfoamp
      xmit.rndmisclfo
      xmit.rndtranspose
      xmit.rndname
   LOOP
   checksum MIDI.XMIT
   tx7.end.sysex
;

DECIMAL


