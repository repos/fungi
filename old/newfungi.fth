\   ,-----------.
\   |  CULTURE  |
\   `-----------'
\   words used to store and manipulate presets

ANEW TASK-newfungi

: inspect ( hex -- ) HEX . DECIMAL ; \ print what's on the stack

: tx7.presetpp ( -- )               \ pretty print current preset (debug)
   MIDI.CLEAR                       \ empty incoming queue
   tx7.presetdump                   \ request a dump of current preset
   MP.RESET                         \ clean start of the MIDI parser
   'C inspect MP-SYSEX-VECTOR !     \ set the sysex vector to inspect's CFA
   MIDI.PARSER.ON                   \ MIDI parser on
   50 MSEC                          \ prevents messy queue
   MIDI.PARSE.MANY                  \ parse all incoming stuff
   MIDI.PARSER.OFF                  \ MIDI parser off
   MP.RESET                         \ clean exit
;

VARIABLE lostbyte                      \ sucks but no idea how to access
: getbyte ( n -- )   lostbyte ! ;      \ the full stack from CFA

CREATE d1 161 ALLOT                    \ two fungi spore donors :)
CREATE d2 161 ALLOT                    
: fungus!   ( byte donor index -- ) + C! ; \ store donor bytes
: fungus@   ( donor index -- )      + C@ ; \ fetch donor bytes
: fungusdump  ( donor -- )      160 DUMP ; \ pretty print a stored donor
                                           \ last byte omitted because of
                                           \ aesthetics :)
                                           \ (it's the checksum byte that
                                           \ we do not use anyway)

HEX

: tx7.senddonor ( donor -- )     \ test word to see if we can properly
   tx7.start.sysex               \ dump and restore the same preset
   00 MIDI.XMIT
   00 MIDI.XMIT
   01 MIDI.XMIT
   1B MIDI.XMIT
   0 SWAP 5 +           \ 5 bytes offset to jump to data address
   9B 0                 \ 155 bytes, 9B in HEX
   DO
      DUP I fungus@ DUP MIDI.XMIT
      ROT + SWAP
      2 MSEC
   LOOP
   DROP
   checksum MIDI.XMIT
   tx7.end.sysex
;

: pickgene ( index -- byte )  \ randomly pick a byte from either d1 or d2
   d1 d2 pickone              \ donors at the given index (param number)
   5 + swap fungus@           \ 5 bytes offset to jump to paramaters
;

VARIABLE opoffset
: opoffset! ( I -- )       15 * opoffset ! ; \ 21 params, 15 in HEX
: opoffset@ ( -- offset)   opoffset @ ;

: xmit.pickopegscl ( n -- n )
   B 0                           \ 0-10, B in HEX
   DO
      I opoffset@ + 
      pickgene DUP MIDI.XMIT   \ 11 parameters op EGs + misc scale stuff
      add+del 
   LOOP
;

: xmit.pickcurve ( n -- n )    \ 2 combined keyboard level scale parameters
   B opoffset@ + pickgene  \ param 11, left curve, B in HEX       000--bb
   C opoffset@ + pickgene  \ param 12, right curve, C in HEX      000bb--
   shift2 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.picktune ( n -- n )     \ 2 combined tuning parameters
   D opoffset@ + pickgene  \ param 13, osc rate scale, D in HEX   ----bbb
   14 opoffset@ + pickgene \ param 20, osc detune, 14 in HEX      bbbb---
   shift3 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.picksense ( n -- n )    \ 2 combined sensitivity parameters
   E opoffset@ + pickgene  \ param 14, amp mod sens, E in HEX     00---bb
   F opoffset@ + pickgene  \ param 15, key vel sens, F in HEX     00bbb--
   shift2 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.pickoutlvl ( n -- n )
   10 opoffset@ + pickgene    \ param 16, output level, 10 in HEX
   DUP MIDI.XMIT
   add+del
;

: xmit.pickfreq ( n -- n )     \ 2 combined osc parameters
   11 opoffset@ + pickgene    \ param 17, osc mode, 11 in HEX     0-----b
   12 opoffset@ + pickgene    \ param 18, osc c freq, 12 in HEX   0bbbbb-
   shift1 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.pickfreqfine ( n -- n )
   13 opoffset@ + pickgene    \ param 19, frequency fine, 13 in HEX
   DUP MIDI.XMIT
   add+del
;

: xmit.pickpitch ( n -- n )
   7E pickgene DUP MIDI.XMIT add+del \ param 126, pitch EG rate 1, 7E in HEX
   7F pickgene DUP MIDI.XMIT add+del \ param 127, pitch EG rate 2, 7F in HEX
   80 pickgene DUP MIDI.XMIT add+del \ param 128, pitch EG rate 3, 80 in HEX
   81 pickgene DUP MIDI.XMIT add+del \ param 129, pitch EG rate 4, 81 in HEX
   82 pickgene DUP MIDI.XMIT add+del \ param 130, pitch EG lvl 1, 82 in HEX
   83 pickgene DUP MIDI.XMIT add+del \ param 131, pitch EG lvl 2, 83 in HEX
   84 pickgene DUP MIDI.XMIT add+del \ param 132, pitch EG lvl 3, 84 in HEX
   85 pickgene DUP MIDI.XMIT add+del \ param 133, pitch EG lvl 4, 85 in HEX
;

: xmit.pickalgo ( n -- n )
   86 pickgene    \ param 134, algorithm select, 86 in HEX
   DUP MIDI.XMIT
   add+del
;

: xmit.pickfbsync ( n -- n )   \ 2 combined osc parameters
   87 pickgene    \ param 135, feedback, 87 in HEX       000-bbb
   88 pickgene    \ param 136, osc key sync, 88 in HEX   000b---
   shift3 bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.picklfo ( n -- n )
   89 pickgene DUP MIDI.XMIT add+del \ param 137, LFO speed, 89 in HEX
   8A pickgene DUP MIDI.XMIT add+del \ param 138, LFO delay, 8A in HEX
   8B pickgene DUP MIDI.XMIT add+del \ param 139, LFO ptch md dph, 8B in HEX
   8C pickgene DUP MIDI.XMIT add+del \ param 140, LFO amp md dpth, 8C in HEX
;

: xmit.pickmisclfo ( n -- n )  \ 3 combined LFO parameters
   8D pickgene \ param 141, LFO sync, 8D in HEX          ------b
   8E pickgene \ param 142, LFO waveform, 8E in HEX      ---bbb-
   8F pickgene \ param 143, LFO pmod sens, 8F in HEX     bbb----
   shift4 swap
   shift1 bin+ bin+
   DUP MIDI.XMIT
   add+del
;

: xmit.picktranspose ( n -- n )
   90 pickgene    \ param 144, transpose, 90 in HEX
   DUP MIDI.XMIT
   add+del
;

: xmit.pickname ( n -- n )
   91 pickgene DUP MIDI.XMIT add+del   \ param 145, voice name 1, 91 in HEX
   92 pickgene DUP MIDI.XMIT add+del   \ param 146, voice name 2, 92 in HEX
   93 pickgene DUP MIDI.XMIT add+del   \ param 147, voice name 3, 93 in HEX
   94 pickgene DUP MIDI.XMIT add+del   \ param 148, voice name 4, 94 in HEX
   95 pickgene DUP MIDI.XMIT add+del   \ param 149, voice name 5, 95 in HEX
   96 pickgene DUP MIDI.XMIT add+del   \ param 150, voice name 6, 96 in HEX
   97 pickgene DUP MIDI.XMIT add+del   \ param 151, voice name 7, 97 in HEX
   98 pickgene DUP MIDI.XMIT add+del   \ param 152, voice name 8, 98 in HEX
   99 pickgene DUP MIDI.XMIT add+del   \ param 153, voice name 9, 99 in HEX
   9A pickgene DUP MIDI.XMIT add+del   \ param 154, voice name 10, 9A in HEX
;

: tx7.newpresets ( -- )       \ generates an offspring from d1 and d2
   tx7.start.sysex
   00 MIDI.XMIT
   09 MIDI.XMIT
   20 MIDI.XMIT
   00 MIDI.XMIT
   0
   20 0                          \ 32 presets, 20 in HEX
   DO
      6 0                           \ 6 OPs, 6 in HEX
      DO
         I opoffset!
         xmit.pickopegscl
         xmit.pickcurve
         xmit.picktune
         xmit.picksense
         xmit.pickoutlvl
         xmit.pickfreq
         xmit.pickfreqfine
      LOOP
      xmit.pickpitch
      xmit.pickalgo
      xmit.pickfbsync
      xmit.picklfo
      xmit.pickmisclfo
      xmit.picktranspose
      xmit.pickname
   LOOP
   checksum MIDI.XMIT
   tx7.end.sysex
;

DECIMAL


