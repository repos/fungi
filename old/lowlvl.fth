\    ,--------------------.
\    |  M!D! SYSEX STUFF  |
\    `--------------------'
\
\     Changes to a preset can be made in 3 ways:
\      - specific live parameter change for currently active
\      - bulk upload of a whole new preset
\      - bulk upload of 32 whole new presets
\     Only the last option writes the changes to the DX7/TX7 memory.
\     The first twos are performance changes only, they will disappear
\     once a new preset is selected and/or if the synth is turned off.
\     Whyyyyyyyyyyy.....

HEX

: tx7.start.sysex ( -- )
   MIDI.START.SYSEX
   43 MIDI.XMIT
;

: tx7.end.sysex ( -- )
   MIDI.END.SYSEX
   MIDI.FLUSH           \ HMSL doc recommends to flush the buffer
;

\ parameter changes (temporary)
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 10  sub-status (1) and channel number (0, ie MIDI channel 1)
\ **  parameter group
\ **  parameter number
\ **  data byte
\ F7  status byte - end sysex

\ tx7.sendparam is a handy word to quickly change any parameter live
\ of the currently active preset. See section L!VE PARAMETER CTRL for
\ some examples.

: tx7.sendparam ( group parameter data -- ) 
   tx7.start.sysex
   10 MIDI.XMIT
   SWAP ROT
   MIDI.XMIT MIDI.XMIT MIDI.XMIT
   tx7.end.sysex
;

\ dump request
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 20  sub-status (2) and channel number (0, ie MIDI channel 1)
\ **  format number: 1, 2, 9, 125 ???
\ F7  status byte - end sysex

: tx7.presetdump ( -- )
   tx7.start.sysex
   20 MIDI.XMIT
   00 MIDI.XMIT
   tx7.end.sysex
;

\ bulk data for 1 preset (temporary)
\ F0  status byte - start sysex
\ 43  vendor ID (Yamaha is 67, 43 in HEX)
\ 00  sub-status (0) and channel number (0, ie MIDI channel 1)
\ 00  format number (0, ie 1 preset)
\ 01  byte count MS byte
\ 1B  byte count LS byte (155, 1 preset)
\ **  data byte 1
\ **  ...
\ **  data byte 155
\ **  checksum (masked 2's complement of sum of 155 bytes)
\ F7  status byte - end sysex

\ TODO: explain the checksum algo here

: tx7.testpreset ( -- )  \ TEST only, generates a temp new preset
   tx7.start.sysex
   00 MIDI.XMIT
   00 MIDI.XMIT
   01 MIDI.XMIT
   1B MIDI.XMIT
   0
   9B 0  \ 155 bytes, 9B in HEX
   DO
      64 CHOOSE DUP MIDI.XMIT    \ not all param are 0-99 -> it's a TEST
      +
      2 MSEC
   LOOP
   checksum MIDI.XMIT
   tx7.end.sysex
;

\ helper word to keep adding what's on the stack and delay operations
\ a bit because otherwise the TX7 can't keep up - not sure if it's
\ the synth or the MIDIHUB that's in the MIDI chain
: add+del ( n -- n )  + 4 MSEC ;

DECIMAL

