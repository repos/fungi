\ FUNG!

\    ,------------.
\    |  DEFAULTS  |
\    `------------'
\

ANEW TASK-main

VARIABLE current-preset
1 current-preset !

1 MIDI.CHANNEL!               \ send/receive on channel 1
current-preset @ MIDI.PRESET  \ load preset 1
1000 MIDI-PARSE-MAX !         \ increase the MIDI.PARSE.MANY queue

\ Note: MIDI.PRESET only works with the TX7 when:
\ - DATA.ENTRY.VOL is ON
\ - the TX7 is in CMB mode
\ why? we'll probably never know...
\ but most likely Yamaha deviating from MIDI conventions
\

INCLUDE 0xA:fungi/utils.fth
INCLUDE 0xA:fungi/lowlvl.fth
INCLUDE 0xA:fungi/rndfungi.fth
INCLUDE 0xA:fungi/newfungi.fth
INCLUDE 0xA:fungi/live.fth

\   ,-------------. 
\   |  MA!N WORDS |
\   `-------------' 
\    most of the time that will be the only
\    words needed to grow and experiment with
\    new fungi

: welcome
   memprotectoff
   ." The following words are available: " CR
   ." randomfungi ( -- ) fill TX7 with new random presets " CR
   ." savefungus ( donor -- ) save current preset as donor " CR
   ." newfungi ( -- ) fill TX7 with offspring presets from donors " CR
;

welcome
