\   ,-----------------------.
\   |  L!VE PARAMETER CTRL  |
\   `-----------------------'
\   debug and helper words based on tx7.sendparam
\   it's all temporary changes and used mostly to 
\   test a few things live
   
ANEW TASK-live

\ interface control

: individual ( -- )    \ switch to IND individual mode
   17 4 1 tx7.sendparam    \ 10001, 17 in DEC
;

: combined ( -- )      \ switch to CMB combined mode
   17 4 0 tx7.sendparam    \ 10001, 17 in DEC
;

: memprotectoff ( -- ) \ disable memory protection
   17 7 0 tx7.sendparam    \ 10001, 17 in DEC
;

: allopson  ( -- )   1 27 63 tx7.sendparam ; \ turn on every OPs
: allopsoff ( -- )   1 27  0 tx7.sendparam ; \ turn off every OPs

: quietnow ( -- )
   allopsoff
   16 0 DO 32 MIDI.NOTE 2 MSEC MIDI.LASTOFF LOOP
   allopson
;

: kbreset ( op -- )    \ normalise keyboard level scaling for an OP
   >R
   0 8 21 R@ * + 99 tx7.sendparam   \ keyboard level scaling break point
   0 9 21 R@ * + 99 tx7.sendparam   \ keyboard level scaling left depth
   0 10 21 R@ * + 99 tx7.sendparam  \ keyboard level scaling right depth
   0 11 21 R@ * + 3 tx7.sendparam   \ keyboard level scaling left curve
   0 12 21 R@ * + 3 tx7.sendparam   \ keyboard level scaling right curve
   0 13 21 R@ * + 7 tx7.sendparam   \ keyboard rate scaling
   R> DROP
;

: reveal ( -- )            \ normalise keyboard scaling for all OPs
   0 kbreset           \ allows to cheat and peak at the potential
   1 kbreset           \ of a preset that may be hidden by unpractical
   2 kbreset           \ keyboard level scaling settings
   3 kbreset
   4 kbreset
   5 kbreset
;

: tx7.quiet ( -- )   1 MIDI.PRESET ; \ reload preset to exhaust polyphony

\ high-level words
\ the ones printed in the welcome message and providing
\ all the needed interactions

: randomfungi ( -- )
   ." generating 32 new random fungus " CR
   tx7.quiet
   1 current-preset !
   1 27 63 tx7.sendparam   \ turn on every OPs
   tx7.rndpresets
   combined
;

: savefungus ( donor -- )           \ store current preset
   MIDI.CLEAR                       \ empty incoming queue
   tx7.presetdump                   \ request a dump of current preset
   MP.RESET                         \ clean start of the MIDI parser
   'C getbyte MP-SYSEX-VECTOR !     \ tmp storage of the parsed byte
   MIDI.PARSER.ON                   \ MIDI parser on
   50 MSEC                          \ prevents messy queue
   161 0
   DO
      MIDI.PARSE
      DUP
      lostbyte @ swap I fungus!
   LOOP
   DROP
   MIDI.PARSER.OFF                  \ MIDI parser off
   MP.RESET                         \ clean exit
;

: newfungi ( -- )   \ fill TX7 with offspring presets from donors
   ." generating 32 presets derived from d1 and d2" CR
   tx7.quiet
   1 current-preset !
   tx7.newpresets
   combined
;

: fungus+ ( -- )  \ load next preset
   current-preset @
   1 + 32 MIN DUP
   MIDI.PRESET
   current-preset !
;

: fungus- ( -- )  \ load next preset
   current-preset @
   1 - 1 MAX DUP
   MIDI.PRESET
   current-preset !
;


\ debug words

: newalgo ( -- )   1 6 32 CHOOSE tx7.sendparam ;   \ new FM algo

: newname ( -- )        \ generate new preset name
   10 0
   DO
      1 17 I + 128 CHOOSE tx7.sendparam
   LOOP
;

: donorspp ( -- ) d1 fungusdump d2 fungusdump ; \ tmp word print the donors

: preps ( -- ) \ tmp word to automate annoying things
   1 MIDI.PRESET 100 MSEC d1 savefungus
   2 MIDI.PRESET 100 MSEC d2 savefungus
   donorspp
;

