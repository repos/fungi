\    ,---------.
\    |  UT!LS  |
\    `---------'
\
\     some of these are used to do some live debugging and also
\     implement part of Yamaha's checksum algo

ANEW TASK-utils.fth

: 2hex  ( n -- )  HEX . DECIMAL ;
: 2bin  ( n -- )  BINARY . DECIMAL ;
: pickone ( n n -- n )  2 CHOOSE 0= IF SWAP THEN DROP ;

BINARY

: 2scompl   ( bin -- bin )  1111111 XOR 1 + ;   \ two's complement
: 7bitmask  ( bin -- bin )  1111111 AND ;       \ 7bit masking
: checksum  ( bin -- bin )  2scompl 7bitmask ;  \ Yamaha's checksum algo
: shift4    ( bin -- bin )  100 << ;            \ l-bitshift 4
: shift3    ( bin -- bin )  11 << ;             \ l-bitshift 3
: shift2    ( bin -- bin )  10 << ;             \ l-bitshift 2
: shift1    ( bin -- bin )  1 << ;              \ l-bitshift 1
: bin+      ( bin -- bin )  + ;                 \ binary addition

DECIMAL

